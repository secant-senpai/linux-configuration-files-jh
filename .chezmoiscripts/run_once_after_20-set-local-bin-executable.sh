#!/bin/bash

set -euo pipefail

declare -a files=(
    "helix-gpt"
)

BIN="$HOME/.local/bin/"

for i in "${!files[@]}"; do
    if [[ -f "$BIN${files[i]}" ]]; then
        echo "Making file $BIN${files[i]} executable..."
        chmod +x "$BIN${files[i]}"
    fi
done
