#!/bin/bash

# Jason Har (harchunhong@proton.me)
# https://gitlab.com/secant-senpai/linux-configuration-files-jh
#
# SCRIPT: Bookmarks Importer for Android (Termux)
# DESC: Sets up bookmarks search through the Termux Widget.
# DEPENDS: git

cd
repo=~/linux-configuration-files-jh
repo2=~/the-vault-of-secant-senpai

# Checks if the repository is cloned in the correct place...
if [ ! -d $repo ]; then
    git clone git@gitlab.com:secant-senpai/linux-configuration-files-jh.git
else
    cd $repo && git pull && cd
fi

# Dependency management
pkg update && pkg upgrade
pkg install openssh fzf

# Checks if .shortcuts folder is present...
if [ ! -d ~/.shortcuts ]; then
    echo "Create directory .shortcuts..." && mkdir ~/.shortcuts
fi

# Universal search prompt
[ ! -f ~/.shortcuts/search ] && echo "Moving search script..." && cp $repo/scripts/search ~/.shortcuts && chmod +x ~/.shortcuts/search && echo "Please check line 22 of the copied search script located at ~/.shortcuts."

# Bookmarks file
# [ -d ~/.config/qutebrowser ] && echo "Making backup of qutebrowser folder into ~/ConfigsBackup..." && cp -r ~/.config/qutebrowser ~/ConfigsBackup/.config/qutebrowser
[ -f $repo2/qutebrowser/urls ] && echo "Moving bookmarks..." && cp $repo2/qutebrowser/urls ~/urls
[ -f $repo2/qutebrowser/quickmarks ] && echo "Moving quickmarks..." && cp $repo2/qutebrowser/quickmarks ~/quickmarks

echo "DONE !"
