#!/bin/sh

# Jason Har (harchunhong@proton.me)
# https://gitlab.com/secant-senpai/linux-configuration-files-jh
#
# SCRIPT: Universal Bookmarks Search Prompt (Dmenu/Rofi on Linux or Termux on Android)
# DESC: A simple prompt that searches all bookmarks located in qutebrowser's urls file.
#       Also functions as a Google search widget if Termux: Widget is set up.
# DEPENDS: fzf (Termux), dmenu/rofi, default browser set via xdg-settings (Linux)

# Define launcher based on type of device.
[ -z "$LAUNCHER" ] && LAUNCHER="rofi -dmenu "           # On Linux
[ -z "${DISPLAY}" ] && LAUNCHER="fzf --prompt "         # On Android
# [ -z "$LAUNCHER" ] && LAUNCHER="dmenu -l 5 -i -p "    # For those who prefer dmenu

# Defines how to open browser with xdg-open.
localBROWSER="$BROWSER "
# [ -n "$*" ] && localBROWSER="$*"
[ "${DISPLAY}" = ':0' ] && localBROWSER="xdg-open "
[ -z "${DISPLAY}" ] && localBROWSER="xdg-open "

# On Linux change the path to: ~/.config/qutebrowser/bookmarks/urls.
if [ -f ~/urls ]; then
    choice=$( (echo "" && cat ~/urls ) | $LAUNCHER"Search:") || exit 1
else
    choice=$(echo "" | $LAUNCHER -i -p "Search DuckDuckGo:") || exit 1
fi

# Go to bookmarks or search the Internet, depending on the type of input (Uses Google atm, can be changed)
case "$choice" in
    **)
    $localBROWSER"https://www.google.com/"
    exit
    ;;
    http*)
    $localBROWSER"$(echo $choice | awk '{print $1}')"
    exit
    ;;
    *) $localBROWSER"https://www.google.com/search?newwindow=1&sxsrf=AOaemvJOuIl7e3eg-Aqz0L2sMl31tgrNQA%3A1641649220360&source=hp&ei=RJTZYa6-E7_n2roPwfaJ4AI&iflsig=ALs-wAMAAAAAYdmiVKwnTqY3w9Cbloi-roqcOE3CKogj&q=$choice"
       exit
       ;;
esac
