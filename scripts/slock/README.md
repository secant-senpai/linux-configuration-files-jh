# slock - Simple Screen Locker

Simple screen locker utility for X.  
Part of the Suckless Software, creator and maintainer of the DWM window manager.

## Installation
slock uses the patch-build compilation process. This means that the software encourages the user to build from source code every time a change was made to its config. Fortunately, the process is very fast as the overall size of the software is very small. suckless.org provides patch modules that add features to the slock config so you do not have to tinker too much with the code. To start, clone this repository and navigate to the slock folder.

### Configuration
The config.def.h file is all you have to edit before install. The user privileges must be set to the name of the current user.

    static const char *user = "userName";
    static const char *group = "userName";

The color shown on the screen while locked with slock can be customized in the following code:

    static const char *colorname[NUMCOLS] = {
        [INIT] =   "black",     /* after initialization */
        [INPUT] =  "#005577",   /* during input */
        [FAILED] = "#CC3333",   /* wrong password */
        [CAPS] = "red",         /* CapsLock on */
    };

### Build
Edit config.def.h to match your local setup. Patch the required modules with the following command in the terminal:

    patch -p1 < patchModuleHere.diff 

By default two modules which I use were already included in the repo. You do not need to repatch them.
1. [Capscolor module](https://tools.suckless.org/slock/patches/capscolor/)
2. [DPMS module](https://tools.suckless.org/slock/patches/dpms/)

Afterwards enter the following commands to build and install slock as lockscreen.

    rm config.h 
    make clean install

## Running slock

Simply invoke the 'slock' command. To get out of it, enter your password.  
The color of the screen shows the feedback to the user depending on their input:
| Color      | Status               |
|------------|----------------------|
| Blue       | Neutral              |
| Red        | Invalid input        |
| Bright Red | Caps lock is pressed |

## Autolocking Script
Window managers such as i3 and QTile does not lock the screen after some time like GNOME and KDE desktop environments do. Thus I have written an autolocking script which mimics the screen dimming and screen locking functionality and then automates the process using [xidlehook](https://gitlab.com/jD91mZM2/xidlehook) and slock.

The locker script is located at [.config/qtile/scripts](https://gitlab.com/secant-senpai/linux-configuration-files-jh/-/blob/main/.config/qtile/scripts/locker). locker must be made executable by `chmod +x locker` and then (OPTIONAL) added to the autostart script of your preferred window manager.

    # Put everything in 1 line.
    exec xidlehook \ 
        --not-when-fullscreen \ 
        --timer 120 'xrandr --output "$PRIMARY_DISPLAY" --brightness .5' 'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' \ 
        --timer 180 'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; slock' '' \
        # --timer 900 'systemctl suspend' 'slock' # Kindly ensure that the system have enough SWAP RAM before enabling the following line. 

Timer was used to count the number of seconds before the next action is triggered. xrandr is used to change the screen brightness while slock is used as the lockscreen. The suspend function at the end invokes slock so that the system remains locked when the system wakes from sleep.
