# Custom Shell Scripts

This folder serves to store my own scripts which aims to automate frequent tasks which I am too lazy to do. In the future, I plan to make scripts that automate the installing of the config files in this repo into a fresh Linux system. Stay tuned for that!

## Disclaimer

The scripts are designed to work on any Linux system. Before any task is done, an automatic backup script runs to migrate existing config files into a folder called 'ConfigsBackups' located at $HOME. With that said, it is still important to keep your own backups just in case an error comes up which may result in a non-workable system. Use these scripts at your own risk.

## Additional Information

The scripts all consist of a header and a code block that is fully labelled.

The header lists down some important information that one should know before running the script such as the name of the script, overall description of the code and the required dependencies. The user must take note of the comment blocks as some parts of the code requires manual intervention by the user, mostly because of differences in the system that the script runs on. When available I will try to keep the amount of manual editing of the code to a minimum.
