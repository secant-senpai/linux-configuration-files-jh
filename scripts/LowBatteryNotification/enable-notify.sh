#!/bin/sh

# Jason Har (harchunhong@protonmail.com)
# https://gitlab.com/secant-senpai/linux-configuration-files-jh
#
# SCRIPT: Low Battery notification for Window Managers
#         Script taken from https://github.com/rhz/sd-battery-notification/
# DESC: Sets up low battery warnings for non-desktop environments.
# DEPENDS: -

# This repository
repo=~/.dotfiles/linux-configuration-files-jh
# Systemd
sysd=~/.config/systemd/user
# Local bin
localB=~/.local/bin
flag=0

# Move script to appropriate places
if [ -f ./sd_battery_notification.service ]; then
    if [ -f ./sd_battery_notification.timer ]; then
        if [ -f ./sd-battery-notification.sh ]; then
            cd $sysd && ln -s $repo/scripts/LowBatteryNotification/sd_battery_notification{.timer,.service} .
            cd $localB && ln -s $repo/scripts/LowBatteryNotification/sd-battery-notification.sh .
            mv ./low-batt.oga ~
            flag=1
        else
            echo "sd-battery-notification.sh not found! Aborting..."
        fi
    else
        echo "sd_battery_notification.timer not found! Aborting..."
    fi
else
    echo "sd_battery_notification.service not found! Aborting..."
fi

if [flag=0]
then
    echo "No changes made."
else
    systemctl --user start sd_battery_notification.timer
    systemctl --user --all list-timers
    echo "DONE ! Reboot the system to apply changes."
fi
