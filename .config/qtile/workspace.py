from libqtile import hook, layout
from libqtile.command import lazy
from libqtile.config import Group, Key, Match

from key_conf import KEYS
from settings import MOD

GROUPS = []

group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
group_labels = ["一", "二", "三", "四", "五", "六", "七", "八", "九", "十"]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "treetab", "monadtall", "monadtall", "monadtall", "monadtall", "max"]

# group_labels = ["1 ", "2 ", "3 ", "4 ", "5 ", "6 ", "7 ", "8 ", "9 ", "0",]
# group_labels = ["", "", "", "", "", "", "", "", "", "",]
# group_labels = ["α", "β", "γ", "δ", "ε", "ζ", "η", "θ", "ι", "κ",]
# group_labels = ["", "", "", "", "",]
# group_labels = ["Web", "Edit/chat", "Image", "Gimp", "Meld", "Video", "Vb", "Files", "Mail", "Music",]

# group_layouts = ["monadtall", "matrix", "monadtall", "bsp", "monadtall", "matrix", "monadtall", "bsp", "monadtall", "monadtall",]


def init_layout_theme() -> dict:
    return {
        "margin": 10,
        "border_width": 2,
        "border_focus": "#ff00ff",
        "border_normal": "#f4c2c2"
    }


layout_theme = init_layout_theme()


LAYOUTS = [
    layout.MonadTall(margin=8, border_width=2, border_focus="#ff00ff", border_normal="#f4c2c2"),
    layout.MonadWide(margin=8, border_width=2, border_focus="#ff00ff", border_normal="#f4c2c2"),
    # layout.Matrix(**layout_theme),
    layout.Bsp(**layout_theme),
    layout.Floating(**layout_theme),
    # layout.RatioTile(**layout_theme),
    layout.Max(**layout_theme),
    layout.Columns(**layout_theme),
    # layout.Stack(**layout_theme),
    layout.Tile(**layout_theme),
    layout.TreeTab(
        sections=['FIRST', 'SECOND'],
        bg_color='#141414',
        active_bg='#0000ff',
        inactive_bg='#1e90ff',
        padding_y=5,
        section_top=10,
        panel_width=280),
    layout.VerticalTile(**layout_theme),
    layout.Zoomy(**layout_theme)
]

for i in range(len(group_names)):
    GROUPS.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))

for i in GROUPS:
    KEYS.extend([
        # CHANGE WORKSPACES
        Key([MOD], i.name, lazy.group[i.name].toscreen()),
        Key([MOD], "Tab", lazy.screen.next_group()),
        Key([MOD, "shift"], "Tab", lazy.screen.prev_group()),
        Key(["MOD1"], "Tab", lazy.screen.next_group()),
        Key(["MOD1", "shift"], "Tab", lazy.screen.prev_group()),

        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        # Key([MOD, "shift"], i.name, lazy.window.togroup(i.name)),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([MOD, "shift"], i.name, lazy.window.togroup(i.name), lazy.group[i.name].toscreen()),
    ])


floating_types = ["notification", "toolbar", "splash", "dialog"]


FLOATING_LAYOUT = layout.Floating(
    float_rules=[
        *layout.Floating.default_float_rules,
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='file_progress'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(wm_class='confirmreset'),
        Match(wm_class='makebranch'),
        Match(wm_class='maketag'),
        Match(wm_class='Arandr'),
        Match(wm_class='feh'),
        Match(wm_class='Galculator'),
        Match(title='branchdialog'),
        Match(title='Open File'),
        Match(title='pinentry'),
        Match(wm_class='ssh-askpass'),
        Match(wm_class='lxpolkit'),
        Match(wm_class='Lxpolkit'),
        Match(wm_class='yad'),
        Match(wm_class='Yad'),
        Match(wm_class='Cairo-dock'),
        Match(wm_class='cairo-dock'),
    ],
    fullscreen_border_width=0,
    border_width=0
)


# ASSIGN APPLICATIONS TO A SPECIFIC GROUPNAME
# Get window names by executing 'xprop WM_CLASS'
@hook.subscribe.client_new
def assign_app_group(client):
    d = {}

    d["1"] = ["Navigator", "Firefox", "Vivaldi-stable", "Vivaldi-snapshot", "Chromium", "Google-chrome", "Brave", "Brave-browser", "Qutebrowser",
              "navigator", "firefox", "vivaldi-stable", "vivaldi-snapshot", "chromium", "google-chrome", "brave", "brave-browser", "qutebrowser", ]
    d["2"] = ["Atom", "Subl3", "Geany", "Brackets", "Joplin", "Obsidian", "github desktop",
              "atom", "subl3", "geany", "brackets", "joplin", "obsidian", "Github Desktop", ]
    d["3"] = ["Inkscape", "Emacs", "Ristretto", "Code-oss", "VSCodium", "jetbrains-idea-ce", "jetbrains-pycharm-ce",
              "inkscape", "emacs", "ristretto", "code-oss", "vscodium", "Jetbrains-idea-ce", "Jetbrains-pycharm-ce", ]
    d["4"] = ["Gimp", "gimp", "Krita", "krita", "Nitrogen", "nitrogen", "Feh", "feh", ]
    d["5"] = ["Discord", "TelegramDesktop", "Slack", "Meld", "org.gnome.meld",
              "discord", "telegramDesktop", "slack", "meld", "org.gnome.Meld", ]
    d["6"] = ["Vlc", "vlc", "gl", "mpv", ]
    d["7"] = ["VirtualBox Manager", "VirtualBox Machine", "Vmplayer", "Gnome-boxes",
              "virtualbox manager", "virtualbox machine", "vmplayer", "gnome-boxes", ]
    d["8"] = ["pcmanfm", "Nemo", "Caja", "Nautilus", "org.gnome.Nautilus", "Pcmanfm", "Pcmanfm-qt",
              "pcmanfm", "nemo", "caja", "nautilus", "org.gnome.nautilus", "pcmanfm", "pcmanfm-qt", ]
    d["9"] = ["Evolution", "Geary", "Mail", "Thunderbird", "Mailspring",
              "evolution", "geary", "mail", "thunderbird", "mailspring", ]
    d["0"] = ["Spotify", "Pragha", "Clementine", "Deadbeef", "Audacious",
              "spotify", "pragha", "clementine", "deadbeef", "audacious", ]

    wm_class = client.window.get_wm_class()[0]

    for i in range(len(d)):
        if wm_class in list(d.values())[i]:
            group = list(d.keys())[i]
            client.togroup(group)
            client.group.cmd_toscreen()
