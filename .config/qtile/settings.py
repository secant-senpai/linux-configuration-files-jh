import os
from libqtile.command import lazy

# Directory Paths
USER_HOME = os.path.expanduser('~')
ROFI_SCRIPTS = USER_HOME + "/.config/rofi/scripts"
QTILE_SCRIPTS = USER_HOME + "/.config/qtile/scripts"

# Controls
# # mod4 or mod = super key
MOD = "mod4"
MOD1 = "alt"
MOD2 = "control"

# Constants
MY_TERM = "kitty"
STOCK_TERM = "xterm"
VOLUME_STEP = 3
BRIGHTNESS_STEP = 3

# Command Line Items
commands = {
    # System Autostart
    "autostart": [f"{QTILE_SCRIPTS}/autostart.sh"],

    # Audio Controls
    "lowerVolume": lazy.spawn(f"sh {QTILE_SCRIPTS}/audio-controls.sh lower {VOLUME_STEP}"),
    "raiseVolume": lazy.spawn(f"sh {QTILE_SCRIPTS}/audio-controls.sh raise {VOLUME_STEP}"),
    "switchSink": lazy.spawn(f"sh {QTILE_SCRIPTS}/audio-controls.sh switch_sink"),
    "toggleMute": lazy.spawn(f"sh {QTILE_SCRIPTS}/audio-controls.sh mute"),

    # Brightness Controls
    "brightnessUp": lazy.spawn(f"sh {QTILE_SCRIPTS}/brightness-controls.sh raise {BRIGHTNESS_STEP}"),
    "brightnessDown": lazy.spawn(f"sh {QTILE_SCRIPTS}/brightness-controls.sh lower {BRIGHTNESS_STEP}"),

    # Browsers
    "firedragon": lazy.spawn("prime-run firedragon"),
    # "firefox": lazy.spawn("prime-run firefox"),
    "qutebrowser": lazy.spawn("prime-run qutebrowser"),
    "vivaldi": lazy.spawn("vivaldi-stable"),

    "dmenu": lazy.spawn(
        "dmenu_run -i -nb '#191919' -nf '#fea63c' -sb '#fea63c' -sf '#191919' -fn 'NotoMonoRegular:bold:pixelsize=14'"
    ),

    # Flameshot (Screenshot Manager)
    "fsFullScreen": lazy.spawn("flameshot full -p " + USER_HOME + "/Pictures/Screenshots"),
    "fsGui": lazy.spawn("flameshot gui"),

    "garudaWelcome": lazy.spawn("garuda-welcome"),
    "termFileManager": lazy.spawn(MY_TERM + " lfub"),
    "pamac": lazy.spawn("pamac-manager"),
    "pavucontrol": lazy.spawn("pavucontrol"),
    "pcmanfm": lazy.spawn("pcmanfm"),
    "picomToggle": lazy.spawn(QTILE_SCRIPTS + "/picom-toggle.sh"),

    # Playerctl
    "goToNext": lazy.spawn("playerctl next"),
    "goToPrevious": lazy.spawn("playerctl previous"),
    "playerStop": lazy.spawn("playerctl stop"),
    "togglePlayPause": lazy.spawn("playerctl play-pause"),
    # "mpcNext": lazy.spawn("mpc next"),
    # "mpcPrevious": lazy.spawn("mpc prev"),
    # "mpcStop": lazy.spawn("mpc stop"),
    # "mpcToggle": lazy.spawn("mpc toggle"),

    # Redshift
    "redshiftStart": lazy.spawn("redshift -l 3.1569:101.7123 & disown"),
    "redshiftStop": lazy.spawn("kill redshift"),

    # Rofi
    # "rofiDrun": lazy.spawn("rofi -show drun"),
    "rofiLauncher": lazy.spawn(ROFI_SCRIPTS + "/launcher_t7"),
    "rofiPowermenu": lazy.spawn(ROFI_SCRIPTS + "/powermenu_t5"),

    "spotify": lazy.spawn("dex /usr/share/applications/spotify-adblock.desktop"),
    "taskManager": lazy.spawn("lxtask"),

    # Terminal
    "altTerm": lazy.spawn(STOCK_TERM),
    "term": lazy.spawn(MY_TERM),

    # Text Editors
    "emacs": lazy.spawn("emacs"),
    # "joplin": lazy.spawn("joplin-desktop"),
    "obsidian": lazy.spawn("obsidian"),

    # XKill
    "xkill": lazy.spawn("xkill"),
}


# Colours
# Default
def init_colors() -> list[list]:
    return [["#2F343F", "#2F343F"],  # color 0
            ["#2F343F", "#2F343F"],  # color 1
            ["#c0c5ce", "#c0c5ce"],  # color 2
            ["#e75480", "#e75480"],  # color 3
            ["#f4c2c2", "#f4c2c2"],  # color 4
            ["#ffffff", "#ffffff"],  # color 5
            ["#ff0000", "#ff0000"],  # color 6
            ["#62FF00", "#62FF00"],  # color 7
            ["#000000", "#000000"],  # color 8
            ["#c40234", "#c40234"],  # color 9
            ["#141C21", "#141C21"],  # color 10
            ["#ff00ff", "#ff00ff"],  # 11
            ["#4c566a", "#4c566a"],  # 12
            ["#282c34", "#282c34"],  # 13
            ["#212121", "#212121"],  # 14
            ["#e75480", "#e75480"],  # 15
            ["#2aa899", "#2aa899"],  # 16
            ["#abb2bf", "#abb2bf"],  # 17
            ["#81a1c1", "#81a1c1"],  # 18
            ["#56b6c2", "#56b6c2"],  # 19
            ["#c678dd", "#c678dd"],  # 20
            ["#e06c75", "#e06c75"],  # 21
            ["#fb9f7f", "#fb9f7f"],  # 22
            ["#ffd47e", "#ffd47e"],  # 23
            ["#a64d79", "#a64d79"]]  # 24

# Catppuccin Mocha
# https://github.com/catppuccin/catppuccin
def init_colors_mocha() -> list[list]:
    return [["#f5e0dc", "#f5e0dc"],  # color 0
            ["#f2cdcd", "#f2cdcd"],  # 1
            ["#f5c2e7", "#f5c2e7"],  # 2
            ["#cba6f7", "#cba6f7"],  # 3
            ["#f38ba8", "#f38ba8"],  # 4
            ["#eba0ac", "#eba0ac"],  # 5
            ["#fab387", "#fab387"],  # 6
            ["#f9e2af", "#f9e2af"],  # 7
            ["#a6e3a1", "#a6e3a1"],  # 8
            ["#94e2d5", "#94e2d5"],  # 9
            ["#89dceb", "#89dceb"],  # 10
            ["#74c7ec", "#74c7ec"],  # 11
            ["#89b4fa", "#89b4fa"],  # 12
            ["#b4befe", "#b4befe"],  # 13
            ["#cdd6f4", "#cdd6f4"],  # 14
            ["#bac2de", "#bac2de"],  # 15
            ["#a6adc8", "#a6adc8"],  # 16
            ["#9399b2", "#9399b2"],  # 17
            ["#7f849c", "#7f849c"],  # 18
            ["#6c7086", "#6c7086"],  # 19
            ["#585b70", "#585b70"],  # 20
            ["#45475a", "#45475a"],  # 21
            ["#313244", "#313244"],  # 22
            ["#1e1e2e", "#1e1e2e"],  # 23
            ["#181825", "#181825"],  # 24
            ["#11111b", "#11111b"]]  # 25

colors = init_colors()
# colors = init_colors_mocha()


def base(fg='text', bg='dark') -> dict:
    return {'foreground': colors[14], 'background': colors[15]}
    # return {'foreground': colors[24], 'background': colors[23]}
