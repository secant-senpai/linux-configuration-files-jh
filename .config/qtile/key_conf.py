from libqtile.command import lazy
from libqtile.config import Key

from settings import MOD, MOD2, commands

KEYS = [
    # SUPER + FUNCTION KEYS

    Key([MOD], "f", lazy.window.toggle_fullscreen()),
    Key([MOD], "q", lazy.window.kill()),
    Key([MOD], "t", commands["altTerm"]),
    Key([MOD], "v", commands["pavucontrol"]),
    Key([MOD], "space", commands["rofiLauncher"]),
    Key([MOD], "d", commands["dmenu"]),
    Key([MOD], "Escape", commands["xkill"]),
    Key([MOD], "Return", commands["term"]),
    Key([MOD], "KP_Enter", commands["term"]),
    # Key([MOD], "x", lazy.shutdown()),

    # SUPER + SHIFT KEYS

    Key([MOD, "shift"], "Return", commands["termFileManager"]),
    Key([MOD, "shift"], "p", commands["pcmanfm"]),
    Key([MOD, "shift"], "q", lazy.window.kill()),
    Key([MOD, "shift"], "r", lazy.restart()),
    Key([MOD, "shift"], "x", commands["rofiPowermenu"]),

    # CONTROL + ALT KEYS

    Key(["MOD1", "control"], "o", commands["picomToggle"]),
    Key(["MOD1", "control"], "r", commands["redshiftStart"]),
    Key(["MOD1", "control"], "u", commands["redshiftStop"]),

    # ALT + ... KEYS

    Key(["MOD1"], "p", commands["pamac"]),
    Key(["MOD1"], "v", commands["vivaldi"]),
    Key(["MOD1"], "f", commands["firedragon"]),
    Key(["MOD1"], "w", commands["garudaWelcome"]),
    Key(["MOD1"], "o", commands["obsidian"]),
    Key(["MOD1"], "e", commands["emacs"]),
    Key(["MOD1"], "s", commands["spotify"]),

    # CONTROL + SHIFT KEYS

    Key([MOD2, "shift"], "Escape", commands["taskManager"]),

    # SCREENSHOTS

    Key([], "Print", commands["fsGui"]),
    Key([MOD2], "Print", commands["fsFullScreen"]),

    # MULTIMEDIA KEYS - INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", commands["brightnessUp"]),
    Key([], "XF86MonBrightnessDown", commands["brightnessDown"]),

    # MULTIMEDIA KEYS - INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", commands["toggleMute"]),
    Key([], "XF86AudioLowerVolume", commands["lowerVolume"]),
    Key([], "XF86AudioRaiseVolume", commands["raiseVolume"]),
    Key([MOD], "a", commands["switchSink"]),

    Key([], "XF86AudioPlay", commands["togglePlayPause"]),
    Key([], "XF86AudioNext", commands["goToNext"]),
    Key([], "XF86AudioPrev", commands["goToPrevious"]),
    Key([], "XF86AudioStop", commands["playerStop"]),

    # QTILE LAYOUT KEYS
    Key([MOD], "n", lazy.layout.normalize()),
    Key([MOD], "w", lazy.next_layout()),

    # CHANGE FOCUS
    Key([MOD], "Up", lazy.layout.up()),
    Key([MOD], "Down", lazy.layout.down()),
    Key([MOD], "Left", lazy.layout.left()),
    Key([MOD], "Right", lazy.layout.right()),
    Key([MOD], "k", lazy.layout.up()),
    Key([MOD], "j", lazy.layout.down()),
    Key([MOD], "h", lazy.layout.left()),
    Key([MOD], "l", lazy.layout.right()),

    # RESIZE WINDOW UP, DOWN, LEFT, RIGHT
    Key([MOD, "control"], "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([MOD, "control"], "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
        ),
    Key([MOD, "control"], "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([MOD, "control"], "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
        ),
    Key([MOD, "control"], "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([MOD, "control"], "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        ),
    Key([MOD, "control"], "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),
    Key([MOD, "control"], "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        ),

    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([MOD, "shift"], "f", lazy.layout.flip()),

    # FLIP LAYOUT FOR BSP
    Key([MOD, "MOD1"], "k", lazy.layout.flip_up()),
    Key([MOD, "MOD1"], "j", lazy.layout.flip_down()),
    Key([MOD, "MOD1"], "l", lazy.layout.flip_right()),
    Key([MOD, "MOD1"], "h", lazy.layout.flip_left()),

    # MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([MOD, "shift"], "k", lazy.layout.shuffle_up()),
    Key([MOD, "shift"], "j", lazy.layout.shuffle_down()),
    Key([MOD, "shift"], "h", lazy.layout.shuffle_left()),
    Key([MOD, "shift"], "l", lazy.layout.shuffle_right()),

    # TREETAB MOVE UP OR DOWN SECTIONS
    Key([MOD, "control"], "k",
        lazy.layout.section_up(),
        desc='Move up a section in treetab'
        ),
    Key([MOD, "control"], "j",
        lazy.layout.section_down(),
        desc='Move down a section in treetab'
        ),

    # MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([MOD, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([MOD, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([MOD, "shift"], "Left", lazy.layout.swap_left()),
    Key([MOD, "shift"], "Right", lazy.layout.swap_right()),

    # TOGGLE FLOATING LAYOUT
    Key([MOD, "shift"], "space", lazy.window.toggle_floating()),
]
