import os
import socket
from libqtile import bar, widget, qtile
from libqtile.command import lazy
from libqtile.config import Screen

from settings import colors, base, commands, MY_TERM


def init_widgets_defaults() -> dict:
    return dict(font="UbuntuMono Nerd Font Bold",
                fontsize=12,
                padding=2,
                background=colors[1])


WIDGET_DEFAULTS = init_widgets_defaults()


def init_widgets_list() -> list:

    prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

    return [

        widget.Sep(
            linewidth=1,
            padding=2,
            foreground=colors[10],
            background=colors[10]
        ),

        # widget.Image(
        #     filename = "~/.config/qtile/icons/garuda-red.png",
        #     iconsize = 9,
        #     background = colors[10],
        #     mouse_callbacks = {'Button1': lambda : qtile.cmd_spawn('jgmenu_run')}
        # ),

        widget.GroupBox(
            **base(bg=colors[10]),
            font='UbuntuMono Nerd Font',

            fontsize=11,
            margin_y=3,
            margin_x=2,
            padding_y=5,
            padding_x=4,
            borderwidth=3,

            hide_unused=True,
            active=colors[5],
            inactive=colors[6],
            rounded=True,
            highlight_method='block',
            urgent_alert_method='block',
            urgent_border=colors[16],
            this_current_screen_border=colors[20],
            this_screen_border=colors[17],
            other_current_screen_border=colors[13],
            other_screen_border=colors[17],
            disable_drag=True
        ),

        # widget.WindowName(
        #     background=colors[10],
        #     #foreground=colors[9],
        #     fmt='{}',
        #     fontsize=12,
        #     padding=10,
        #     max_chars=60),

        widget.TaskList(
            highlight_method='block',
            icon_size=17,
            max_title_width=300,
            rounded=True,
            padding_x=0,
            padding_y=0,
            margin_y=0,
            font="UbuntuMono Nerd Font Bold",
            fontsize=14,
            border=colors[7],
            foreground=colors[9],
            margin=2,
            txt_floating='🗗  ',
            txt_minimized='>_  ',
            borderwidth=1,
            background=colors[10],
            # unfocused_border = 'border'
        ),

        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground=colors[5],
            background=colors[3],
            padding=0,
            scale=0.7
        ),

        widget.CurrentLayout(
            #   font = "Noto Sans Bold",
            font="UbuntuMono Nerd Font Bold",
            fontsize=12,
            foreground=colors[5],
            background=colors[3]
        ),

        # widget.Net(
        #     #  font="Noto Sans",
        #     font = "UbuntuMono Nerd Font Bold",
        #     fontsize=12,
        #     # Here enter your network name
        #     interface=["wlp3s0"],
        #     format = '  {down} ↓↑ {up}  ',
        #     foreground=colors[5],
        #     background=colors[19],
        #     padding = 0,
        #     ),

        widget.CPU(
            # font="Noto Sans",
            # format = '{MemUsed}M/{MemTotal}M',
            # format = 'CPU: {load_percent}%',
            format='  {load_percent}%  ',
            update_interval=1,
            fontsize=12,
            foreground=colors[9],
            background=colors[22],
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(MY_TERM + " -e bpytop")},
        ),

        widget.Memory(
            # font="Noto Sans",
            # format = 'RAM: {MemUsed: .0f}M /{MemTotal: .0f}M',
            format='  {MemUsed: .2f}{mm}  ',
            update_interval=1,
            fontsize=12,
            measure_mem='G',
            foreground=colors[5],
            background=colors[16],
            mouse_callbacks={'Button1': lambda: qtile.cmd_spawn(MY_TERM + " -e bpytop")},
        ),

        widget.NvidiaSensors(
            foreground=colors[5],
            background=colors[24],
            padding=2,
            fontsize=12,
            gpu_bus_id='01:00.0',
            update_interval=5,
            format='  {temp} °C |'
        ),

        widget.Battery(
            foreground=colors[5],
            background=colors[24],
            padding=2,
            # font = 'FiraCode Nerd Font Regular',
            fontsize=12,
            format=' {percent:2.0%}  ',
            notify_below=15,
            # charge_char = '',
            # discharge_char = '',
            # unknown_char = '='
        ),

        widget.Clock(
            foreground=colors[9],
            background=colors[23],
            fontsize=12,
            format="  %a %B, %d | %I:%M %p  "
        ),

        widget.Systray(
            background=colors[10],
            icon_size=20,
            padding=4
        ),
    ]


def init_screens() -> list[Screen]:
    return [
        Screen(
            top=bar.Bar(
                widgets=init_widgets_list(),
                size=20,
                opacity=0.85,
                background="000000"
            )
        )
    ]

SCREENS = init_screens()
