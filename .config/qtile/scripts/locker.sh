#!/bin/sh

# Jason Har (harchunhong@protonmail.com)
# https://gitlab.com/secant-senpai/linux-configuration-files-jh
#
# SCRIPT: Autolocking Script for Window Managers
# DESC: Mimics the screen dimming and screen locking functionality and automates the process using xidlehook and slock.
# DEPENDS: xidlehook, slock

# Only exported variables can be used within the timer's command.
export PRIMARY_DISPLAY="$(xrandr | awk '/ primary/{print $1}')"

exec xidlehook --not-when-fullscreen --timer 120 'xrandr --output "$PRIMARY_DISPLAY" --brightness .5' 'xrandr --output "$PRIMARY_DISPLAY" --brightness 1' --timer 180 'xrandr --output "$PRIMARY_DISPLAY" --brightness 1; slock' '' # --timer 900 'systemctl suspend' 'slock'
