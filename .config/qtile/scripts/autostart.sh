#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# Start utility applications
~/.config/qtile/scripts/locker.sh &
lxsession &
run nm-applet &
#run pamac-tray &
#numlockx on &
blueman-applet &
run volumeicon &
#flameshot &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &

# Start picom compositor and dunst notification server
#picom --config $HOME/.config/picom/picom.conf &
picom --config $HOME/.config/picom/picom-blur.conf &
dunst &

# Set background
# feh --bg-scale /home/jasonh/Pictures/Wallpapers/14.jpg
feh --bg-max /home/jasonh/Pictures/Wallpapers/GenshinImpact\ 2022-11-02\ 17-43-33.png
#feh --randomize --bg-fill /usr/share/wallpapers/garuda-wallpapers/*
#nitrogen --random --set-zoom-fill &

# Start user applications
run volumeicon &
run discord --start-minimized &         # Discord
# KDE Connect
dex /usr/share/applications/org.kde.kdeconnect.nonplasma.desktop
kdeconnect-indicator &
run deluge &                            # Deluge 
# run thunderbird --new-instance &        # Thunderbird mail client
