import subprocess
from typing import List  # noqa: F401
from libqtile import hook
from libqtile.command import lazy

from bar import WIDGET_DEFAULTS, SCREENS
from key_conf import KEYS
from mouse import MOUSE
from settings import MOD, MY_TERM, commands
from workspace import LAYOUTS, GROUPS, FLOATING_LAYOUT, floating_types


# Global
mod = MOD
keys = KEYS
widget_defaults = WIDGET_DEFAULTS
screens = SCREENS
layouts = LAYOUTS
groups = GROUPS
mouse = MOUSE
floating_layout = FLOATING_LAYOUT
terminal = MY_TERM

@lazy.function
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


# Hooks
@hook.subscribe.startup_once
def start_once():
    subprocess.call(commands["autostart"])


@hook.subscribe.startup
def start_always():
    # Set the cursor to something sane in X
    subprocess.Popen(['xsetroot', '-cursor_name', 'left_ptr'])


@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True


# Additional Configs
auto_fullscreen = True
auto_minimize = True
bring_front_click = False
cursor_warp = False
dgroups_key_binder = None
dgroups_app_rules = []
focus_on_window_activation = "focus"    # or smart
follow_mouse_focus = True

wmname = "Qtile"
