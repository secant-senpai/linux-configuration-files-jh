from libqtile.command import lazy
from libqtile.config import Drag

from settings import MOD

MOUSE = [
     # RESIZE WINDOW
     Drag([MOD], "Button1", lazy.window.set_position_floating(),
          start=lazy.window.get_position()),
     Drag([MOD], "Button3", lazy.window.set_size_floating(),
          start=lazy.window.get_size())
]
