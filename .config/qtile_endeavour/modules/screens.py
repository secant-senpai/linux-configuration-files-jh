from libqtile import bar
from .widgets import *
from libqtile.config import Screen
from .spotify import spotify_info, spotify_ctrl, spotify_next, spotify_prev
import os

terminal = "kitty"

screens = [
    Screen(
        top=bar.Bar(
            [   widget.Sep(padding=3, linewidth=0, background="#2f343f"),
                widget.Image(
                    filename='~/.config/qtile/QTile-logo',
                    margin=3,
                    background="#2f343f", 
                    mouse_callbacks={ 'Button1': lambda: qtile.cmd_spawn("rofi -show drun") }),
                #widget.Sep(padding=3, linewidth=0, background="#2f343f"), 
                widget.GroupBox(
                                disable_drag=True,
                                highlight_method='line',
                                this_screen_border="#5294e2",
                                this_current_screen_border="#fff300",
                                active="#ffffff",
                                inactive="#848e96",
                                background="#2f343f"),
                widget.TextBox(
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f'
                       ),
                widget.Prompt(),
                widget.Spacer(length=5),
                widget.WindowName(foreground='#A0ABAF',fmt='{}'),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Systray(icon_size = 20),
                widget.Spacer(length = 7),
                # widget.CheckUpdates(
                #     update_interval=1800,
                #     distro="Arch_yay",
                #     display_format="{updates} Updates",
                #     foreground='#ffffff',
                #     mouse_callbacks={ 'Button1': lambda: qtile.cmd_spawn('eos-welcome') }
                #     ),
                # widget.Spacer(length = 5),
                widget.TextBox(
                    text = '',
                    padding = 0,
                    fontsize = 28,
                    foreground='#2f343f'
                    ),
                widget.CurrentLayoutIcon(
                    scale=0.70,
                    background = '#2f343f'
                    ),
                widget.TextBox(                                                                    
                        text = '',
                        padding = 0,
                        fontsize = 28,
                        foreground='#2f343f',
                        ),
                widget.CPU(
                    format = 'CPU: {load_percent}%',
                    padding = 0,
                    foreground = '#ffffff',
                    mouse_callbacks={ 'Button1': lambda: qtile.cmd_spawn(terminal + ' -e bpytop') }
                    ),
                widget.TextBox(
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f'
                       ), 
                widget.Memory(
                    format = 'RAM:{MemUsed: .1f} {mm}',
                    measure_mem = 'G',
                    measure_swap = 'G',
                    padding = 0,
                    foreground = '#ffffff',
                    background = '#2f343f',
                    mouse_callbacks={ 'Button1': lambda: qtile.cmd_spawn(terminal + ' -e bpytop') }
                ),
                widget.TextBox(                                                                    
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f',
                       ),
                widget.GenPollText(
                    func = spotify_info,
                    update_interval = 1,
                    padding =  0,
                    # mouse_callbacks={ 'Button1': spotify_ctrl,
                    #                   'Button4': spotify_prev,
                    #                   'Button5': spotify_next }
                ),
                widget.TextBox(
                       text = '',
                       padding = 2,
                       fontsize = 28,
                       foreground='#2f343f'
                       ), 
                volume,
                widget.TextBox(                                                                    
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f',
                       ),
                widget.Battery(
                    padding = 0,
                    #font = 'FiraCode Nerd Font Regular',
                    #fontsize = 28,
                    format = '{percent:2.0%}',
                    notify_below = 15,
                    # charge_char = '',
                    # discharge_char = '',
                    # unknown_char = '='
                    ),
                widget.TextBox(
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f'
                       ),    
                widget.Clock(format=' %d %b %Y %a %I:%M %p',
                             background="#2f343f",
                             foreground='#fff300'),
                widget.TextBox(                   
                       text = '',
                       padding = 0,
                       fontsize = 28,
                       foreground='#2f343f',
                       ),   
                widget.TextBox(
                    text='',
                    font='FiraCode Nerd Font Regular',
                    fontsize = 15,
                    mouse_callbacks= {
                        'Button1':
                        lambda: qtile.cmd_spawn(os.path.expanduser('~/.config/rofi/powermenu.sh'))
                    },
                    foreground='#de612c'
                ),
                # widget.TextBox(
                    # text = ' ',
                    # padding = 0
                    # )
                widget.Spacer(length = 10)
                
            ],
            30,  # height in px
            background="#404552"  # background color
        ), ),
]
