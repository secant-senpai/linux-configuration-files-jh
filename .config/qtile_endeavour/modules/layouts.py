from libqtile import layout
from libqtile.config import Match

layout_theme = {"border_width" : 3,
                "margin" : 8,
                "border_focus" : '#0D55C2',
                "border_normal" : '#492E2D'
               }

layouts = [
    # layout.MonadTall(margin=8, border_focus='#5294e2',
                     # border_normal='#2c5380'),
    layout.MonadTall(**layout_theme),
    #layout.Columns(border_focus_stack='#d75f5f'),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
     layout.Bsp(**layout_theme),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    layout.Zoomy(**layout_theme),
    layout.Floating(**layout_theme)
]

floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='discord'),       # Discord
    Match(wm_class='Thunderbird'),   # Thunderbird
    Match(wm_class='screengrab'),    # Screengrab screenshotting tool
    Match(wm_class='steam'),         # Steam
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
])
