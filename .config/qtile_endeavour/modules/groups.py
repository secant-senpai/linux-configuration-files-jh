from libqtile.config import Key, Group, Match
from libqtile.lazy import lazy
from .keys import keys, mod

#groups = [Group(i) for i in "123456789"]

groups = [
    Group("1", 
    layout="max",
    matches = [Match(wm_class=["firefox"]),
                Match(wm_class=["Chromium"]),
               Match(wm_class=["qutebrowser"])]
    ),
    Group("2",
    layout = "bsp",
    matches = [Match(wm_class=["Emacs"]),
               Match(wm_class=["VSCodium"])]
    ),
    Group("3",
    layout = "bsp",
    matches = [Match(wm_class=["Thunar"])]
    ),
    Group("4"),
    Group("5"),
    Group("6"),
    Group("7",
    matches = [Match(wm_class=["Thunderbird"])]
    ),
    Group("8",
    #matches=[Match("discord")]
    matches = [Match(wm_class=["Blueman-manager"]),
               Match(wm_class=["easyeffects"])]
    ),
    Group("9",
    matches=[Match(["Spotify"])]
    )
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name)),

        Key([mod], "Right", lazy.screen.next_group(),
            desc="Switch to next group"),

        Key([mod], "Left", lazy.screen.prev_group(),
            desc="Switch to previous group"),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"],
            i.name,
            lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name)),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])
