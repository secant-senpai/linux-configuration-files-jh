#!/bin/sh
# Set background
#feh --bg-scale /usr/share/endeavouros/backgrounds/endeavouros-wallpaper.png
feh --bg-scale /home/jasonh/.config/qtile/0018.jpg

# Start picom compositor and dunst notification server
picom & disown --experimental-backends --vsync
dunst & disown

# Low battery notifier
~/.config/qtile/scripts/check_battery.sh & disown

# Start EOS welcome
eos-welcome & disown

/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 & disown # start polkit agent from GNOME

locker & disown                    # autolock screen
nm-applet & disown                 # Network manager module
easyeffects & disown               # EasyEffects
#fcitx-autostart                    # Fcitx Input Method
dex /usr/share/applications/org.kde.kdeconnect.nonplasma.desktop # KDE Connect
discord --start-minimized & disown # Discord
#premid & disown                    # PreMID
deluge & disown                     # Deluge
blueman-manager & disown            # Bluetooth module
thunderbird --new-instance & disown # Thunderbird mail client
kitty                               # Start a terminal instance
