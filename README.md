# Linux Personalized Configuration Files 

Every dot file, sh file and important data created/modified in my Linux adventure, for easy recreation of my desktop during reinstalls or distrohopping.

## About this repository

This repository consists of configuration files for my own personalized usage. The repo is divided into several categories which represent the type of application the files are corresponded to. With these files, anyone can recreate my customizations of the OS in Arch Linux. At present, the configs are based on the current daily driver that I use, thus backups of your system are required before using my files in case of any incompatibilities with your OS. 

### Screenies
This is the QTile Edition.

![My Desktop.](/demo/Desktop.png "Desktop")

![Tiling Showcase.](/demo/Desktop%202.png "Tiling Showcase")

### Applying Configs
This repository is compatible with [Chezmoi.io](https://github.com/twpayne/chezmoi). You can install the configs by using the following one-liner command:

```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/secant-senpai/linux-configuration-files-jh.git
```

* You can find the cloned contents in the location `$HOME/.local/bin/chezmoi`.
* Remove the `--apply` flag to keep the configs in editable mode, without installing on your machine.

## Licensing

All of my dotfiles (and my dotfiles only) are available under the GNU GPLv3 License. Please consult /secant-senpai/linux-configuration-files-jh/-/blob/main/LICENSE for more information. In short: you are free to access, edit and redistribute all of my dotfiles under the same license and as allowed by the license, and if you messed up your main workstation, it’s your own responsibility.
